(in-package :sudoku)

(declaim (inline xy-to-linear))
(defun xy-to-linear (col row &optional (multiple size))
  (+ col (* row multiple)))


(defun quad (which)
  "returns a list of coordinates for the quadrant."
  (multiple-value-bind (nrow ncol) (truncate which sqrt-size)
    (let ((cells (iter (for row below sqrt-size)
                   (nconcing
                     (iter (for col below sqrt-size)
                       (collect (xy-to-linear 
                                  (+ (* ncol sqrt-size) col)
                                  (+ (* nrow sqrt-size) row))))))))
      (make-area :name (format nil "quad-~d" which)
                 :cells cells))))

(defparameter *quad-areas* (mapcar #'quad (alexandria:iota size)))

(defun areas-to-cell-index (areas store-index?)
  (let ((idxs (make-array +total-cells+ 
                          :initial-element 0)))
    (map nil (lambda (area a-n)
               (dolist (pos (area-cells area))
                 (setf (aref idxs pos)
                       (ecase store-index?
                         (:index a-n)
                         (:area area)
                         (:both (list a-n area))))))
         areas
         (alexandria:iota (length areas)))
    idxs))

(defparameter *reverse-quad-areas* (areas-to-cell-index *quad-areas* :index))

(defun col (which)
  "returns a list of coordinates for the given column"
  (assert (< -1 which size))
  (make-area :name (format nil "col-~d" which)
              :cells (mapcar (lambda (x)
                               (+ (* x size)
                                  which))
                             (alexandria:iota size))))

(defvar *col-areas* (mapcar #'col (alexandria:iota size)))

(defun row (which)
  "returns a list of coordinates for the given row."
  (assert (< -1 which size))
  (make-area :name (format nil "row-~d" which)
              :cells (mapcar (lambda (x)
                               (+ (* which size)
                                  x))
                             (alexandria:iota size))))

(defvar *row-areas* (mapcar #'row (alexandria:iota size)))



(defparameter *cell-to-default-areas* (map 'vector
                                           (lambda (pos)
                                             (list (find-if (lambda (area)
                                                              (find pos (area-cells area)
                                                                    :test #'=))
                                                            *row-areas*)
                                                   (find-if (lambda (area)
                                                              (find pos (area-cells area)
                                                                    :test #'=))
                                                            *col-areas*)))
                                           (alexandria:iota +total-cells+)))

(defun areas-for-cell (board pos)
  (let ((cell (board-cell board pos)))
    (list* (cell-area cell)
           (aref *cell-to-default-areas* pos))))

(defparameter *rows-and-cols* (concatenate 'list *row-areas* *col-areas*))
