(in-package :sudoku)

;;; For the standard sudoku,
;;; we need to have 9 bits (1 for each possible digit)
;;; plus 4 to identify one of the 9 areas.
;;; That's 4608 possibilities, or < (expt 69 2).

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter *base69* (flet ((a-z (base)
                                  (iter (for i below 26) (collect (code-char (+ base i))))))
                           (concatenate 'base-string 
                                        "0123456789"
                                        (a-z (char-code #\A))
                                        (a-z (char-code #\a))
                                        "._!|-*/")))
  (defparameter *base69-reverse* (let ((x (make-array 128 
                                                      :element-type '(signed-byte 8)
                                                      :initial-element -1)))
                                   (iter (for i upfrom 0)
                                     (for ch in-vector *base69*)
                                     (setf (aref x (char-code ch)) i))
                                   x))
  (defconstant +base69-base+ (length *base69*))
  (assert (= 69 +base69-base+))
  )

(defun base69-char (num)
  (aref *base69* num))

(defun encode-to-2-base69-chars (num)
  (multiple-value-bind (h l) (floor num +base69-base+)
    (values (base69-char h)
            (base69-char l))))

(defun encode-to-base69-string-2 (num)
  (make-array 2 
              :element-type 'base-char
              :initial-contents (multiple-value-list (encode-to-2-base69-chars num))))

#+(or)
(list (encode-in-base69 0)
      (encode-in-base69 (1- (*  1 69)))
      (encode-in-base69 (1+ (*  1 69)))
      (encode-in-base69 (1+ (*  2 69)))
      (encode-in-base69 (1- (* 69 69))))


(defun base69-num (c)
  (aref *base69-reverse* (char-code c)))

(defun decode-base69 (stream count)
  (iter (with r = 0)
    (repeat count)
    (for c = (the base-char (read-char stream)))
    (for n = (base69-num c))
    (setf r (+ (* r +base69-base+)
               n))
    (finally (return r))))

#+(or)
(list (with-input-from-string (s "/")
        (decode-base69 s 1))
      (with-input-from-string (s "//")
        (decode-base69 s 2)))
