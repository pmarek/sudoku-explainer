* Sudoku-explainer

This project aims not to solve Sudokus (although it does that too, of course),
but to explain _how_ to solve a given Sudoku - by analysing the situation and
explaining possible next moves.

It does _not_ do brute-force runs (by trying possible numbers).

