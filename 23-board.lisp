(in-package :sudoku)

(defstruct (board
             (:copier nil))
  (cells (error "") :type (simple-array cell (#. +total-cells+)))
  (areas (error "") :type (simple-array area (#. size))))

(defun board-fix-cell-areas (cell-vector areas set-idx?)
  (let ((index (areas-to-cell-index areas :both)))
    (iter (for pos below +total-cells+)
      (for cell = (aref cell-vector pos))
      (for (area-idx area) = (aref index pos))
      (when set-idx?
        (setf (cell-area-idx cell) area-idx))
      (setf (cell-area cell) area))))


;; base69 has >6 bits per digit
(defconstant +prefix-bits-per-digit+ 6)
(defconstant +prefix-bits-max-num+ (expt 2 +prefix-bits-per-digit+))
(assert (>= +base69-base+ +prefix-bits-max-num+))

(defconstant +finished-cells-prefix-len+ (ceiling +total-cells+ +prefix-bits-per-digit+))

(defun deserialize-board (input)
  (let ((had-areas? nil)
        (cells (make-array +total-cells+
                           :element-type 'cell
                           :initial-element *dummy-cell*))
        (areas (make-array size 
                           :initial-element *dummy-area*
                           :element-type 'area))
        (finished-cells 0))
    (flet ((register-area (pos a-n)
             (let ((area (aref areas a-n)))
               (if (eq area *dummy-area*)
                   (setf (aref areas a-n)
                         (make-area :name (format nil "area-~d" pos)
                                    :cells (list pos)))
                   (push pos (area-cells area))))))
      (with-input-from-string (s input)
        ;; Read DONE? bits for each cell
        (iter (for i below +finished-cells-prefix-len+)
          (for ch = (read-char s))
          (for num = (base69-num ch))
          (assert (< num +prefix-bits-max-num+))
          (for index = (* i +prefix-bits-per-digit+))
          (setf (ldb (byte +prefix-bits-per-digit+ index)
                     finished-cells)
                num))
        ;; Read possible numbers bitmask and area index
        (iter 
          (for pos below +total-cells+)
          (for n = (decode-base69 s 2))
          (for poss = (logand +cell-all-possible+ n))
          (for area-num = (ash n (- size)))
          ;(format t "~d: 0~o, ~d~%" pos poss area-num)
          (when (plusp area-num)
            (setf had-areas? t))
          (setf (aref cells pos)
                (make-cell :possible poss
                           :pos pos
                           :done? (if (logbitp pos finished-cells)
                                      (only-one-possibility-left poss))
                           :area-idx area-num
                           :area *dummy-area*))
          (register-area pos area-num)
          (finally
            (return
              (let ((areas (if had-areas?
                               areas
                               *quad-areas*)))
                (board-fix-cell-areas cells areas had-areas?)
                (make-board :cells cells
                            :areas (coerce areas 'vector))))))))))


(defmethod serialize-board ((cell-array simple-array) stream)
  (check-type cell-array (simple-array cell (#. +total-cells+)))
        ;; Write DONE? bits for each cell
  (iter (with finished-mask = 0)
    (for pos below +total-cells+)
    (setf (ldb (byte 1 pos)
               finished-mask)
          (if (cell-done? (board-cell cell-array pos))
              1
              0))
    (finally
      (iter (for i below +finished-cells-prefix-len+)
        (for index = (* i +prefix-bits-per-digit+))
        (for num = (ldb (byte +prefix-bits-per-digit+ index)
                        finished-mask))
        (for ch = (base69-char num))
        (princ ch stream))))
  ;; Write possible numbers and area code
  (iter 
    (for pos below +total-cells+)
    (for cell = (aref cell-array pos))
    (for num = (logior (cell-possible cell)
                       (ash (cell-area-idx cell) size)))
    (multiple-value-bind (h l) (encode-to-2-base69-chars num)
      (princ h stream)
      (princ l stream))))

(defmethod serialize-board ((board board) stream)
  (serialize-board (board-cells board) stream))


(defmethod board-cell ((board array) pos)
  (aref board pos))

(defmethod board-cell ((board board) pos)
  (board-cell (board-cells board) pos))


(defun make-full-board (&optional areas)
  (let ((cells (map 'vector 
                    (lambda (i)
                      (make-cell :pos i
                                 :possible +cell-all-possible+))
                    (alexandria:iota +total-cells+))))
    (if areas
        (board-fix-cell-areas cells areas t)
        (board-fix-cell-areas cells *quad-areas* nil))
    (make-board :cells cells
                :areas (or areas (coerce *quad-areas* 'vector)))))

(defmethod copy-board-cells ((board array))
  (map 'vector #'copy-cell board))

(defmethod copy-board-cells ((board board))
  (copy-board-cells (board-cells board)))

(defun areas-for-board (board)
  ;; possibly irregular areas
  (concatenate 'list
               (coerce (board-areas board) 'list)
               ;; and rows and columns
               *rows-and-cols*))

(defun copy-board (board)
  ;; Areas should be R/O anyway
  (make-board :areas (board-areas board)
              :cells (map 'vector 
                          (lambda (i c)
                            (with-slots (possible area-idx done?) c
                              (make-cell :pos i
                                         :possible possible
                                         :area-idx area-idx
                                         :area     (aref (board-areas board) area-idx)
                                         :done?    done?)))
                          (alexandria:iota +total-cells+)
                          (board-cells board))))

(defun board-done? (board)
  (every #'cell-done? (board-cells board)))
