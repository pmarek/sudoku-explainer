(in-package :sudoku)


(defparameter *solver-algos* nil)
(defmacro defsolver (name args &body body)
  `(progn
     (pushnew ',name *solver-algos* :test #'eq)
     (defun ,name ,args ,@ body)))


(define-condition return-a-step ()
  ((kind         :initarg :kind         :reader solution-kind)
   (class-cb     :initarg :callback     :reader solution-cell-class)
   (new-state    :initarg :new-state    :reader solution-new-state)
   (changes      :initarg :changes      :reader solution-changes)
   (title-cb     :initarg :title        :reader solution-title)))


(defsolver only-one-number-left-in-cell (board)
  (dotimes (pos +total-cells+)
    (let ((cell (board-cell board pos)))
      (when (not (cell-done? cell))
        (let ((must-be (only-one-possibility-left cell)))
          (when must-be
            (let* ((new-cells (copy-board-cells board)))
              (multiple-value-bind (changed changes)
                  (set-number must-be pos new-cells)
                (flet
                    ((cell-class-fn (cell-num)
                       (format nil "~:[~;cause~] ~:[~;effect~]"
                               (= pos cell-num)
                               (member cell-num changed)))
                     (title ()
                       (cl-who:with-html-output-to-string (*standard-output*)
                         (:span 
                          "As "
                          (:span :class "cause" "cell " (cl-who:str pos))
                          " can only be " (cl-who:str (bit-to-name must-be)) ","
                          "cells "
                          (:span :class "effect"
                           (cl-who:fmt "~{~a~^, ~}"
                                       (mapcar #'bit-to-name changed)))
                          " get affected as well."))))
                  (signal 'return-a-step
                          :kind 'only-one-number-left-in-cell
                          :changes changes
                          :callback  #'cell-class-fn
                          :title     #'title
                          :new-state new-cells))))))))))


(defsolver only-one-place-left-in-area (board)
  (dolist (area (areas-for-board board))
    (let ((counts (make-array size
                              :element-type 'list
                              :initial-element ())))
      (dolist (pos (area-cells area))
        (dotimes (num size)
          (when (is-allowed? num pos board)
            (push pos
                  (aref counts num)))))
      (loop for num from 0
            for cells across counts
            for pos = (first cells)
            ;; Only one position for this symbol?
            do (when (and (= (length cells) 1)
                          ;; and not already set there?
                          (not (cell-done? (board-cell board pos))))
                 (let* ((new-cells (copy-board-cells board)))
                   (multiple-value-bind (changed% changes)
                       (set-number num pos new-cells)
                     (let ((changed (remove pos changed%)))
                       (flet
                           ((cell-class-fn (cell-num)
                              (format nil "~:[~;cause~] ~:[~;effect~] ~:[~;expl~]"
                                      (= cell-num pos)
                                      (member cell-num changed)
                                      (member cell-num (area-cells area))))
                            (title ()
                              (cl-who:with-html-output-to-string (*standard-output*)
                                (:span 
                                 (:b 
                                  "Number "
                                  (cl-who:str (bit-to-name num)))
                                 " must be in "
                                 (:span :class "cause"
                                  "cell " (cl-who:str pos))
                                 ", as this is the only possible place in "
                                 (:span 
                                  ;; For row or cols, we need to highlight;
                                  ;; for quad or irregular areas, we could use the area class.
                                  ;; That saves one kind of pattern - but would be inconsistent.
                                  :class "expl"
                                  #+(or)
                                  (format nil "area-~d"
                                          (cell-area-idx (board-cell board pos)))
                                  (cl-who:str (area-name area)))
                                 (when changed
                                   (cl-who:htm
                                     "; cells "
                                     (:span :class "effect"
                                      (cl-who:fmt "~{~a~^, ~}" (reverse changed)))
                                     " get affected as well"))
                                 "."))))
                         (signal 'return-a-step 
                                 :kind 'only-one-place-left-in-area
                                 :changes changes
                                 :callback   #'cell-class-fn
                                 :title      #'title
                                 :new-state  new-cells))))))))))

;; See ALEXANDRIA-2 soon
(defun map-permutations* (chunk list &key (sets 2) (min-length 0))
  "Splits LIST into all possible combinations of SETS sets
  with each at least MIN-LENGTH elements,
  calling CHUNK with the sets as &REST arguments.

  The order of the elements within the sets is undefined;
  CHUNK must not modify the incoming lists.

  This is a recursive function; stack usage is proportional to (LENGTH LIST).

  There will be no duplicate calls, ie. no (A B) and (B A) calls for sets A and B."
  (let ((split-up-sets (make-list sets)))
        (labels 
            ((rec (c cons)
               (if cons
                   (destructuring-bind (current . next) cons
                     ;; To avoid duplicate results (sets A B and B A),
                     ;; we require the first elements to be in the first few sets only
                     (let ((next-c (1+ c)))
                       (dotimes (i (min next-c sets))
                         (push current (elt split-up-sets i))
                         (rec next-c next)
                         (pop (elt split-up-sets i)))))
                   (let ((lengths (mapcar #'length split-up-sets)))
                     (when (or (= min-length 0)
                               (every (lambda (x) (>= x min-length))
                                      lengths))
                       ;; ... and that all empty sets must be at the end.
                       (let ((first-nil (position          0 lengths))
                             (last-arg  (position-if #'plusp lengths :from-end t)))
                         (when (> (or first-nil sets) last-arg)
                           (apply chunk split-up-sets))))))))
      (rec 0 list))))

(defsolver set-of-cells-complete (board)
  "If 3 cells may only contain (1 2), (2 3), and (1 3),
  then all other cells in the same area MUST NOT contain these numbers any more.
  A kind of generalization of the \"only place left\" above."
  (dolist (area (areas-for-board board))
    (labels 
        ((check-complete (set rest)
           (let* ((superset (cell-union set))
                  (total (how-many-possibilities-left superset))
                  (new-cells (copy-board-cells board)))
             (when (< total (length set))
               (error "not enough numbers for the cells ~s?!" set))
             (when (= total (length set))
               (let* ((changes 0)
                      (changed (iter (for cell in rest)
                                 (for chg = (disallow-multiple superset 
                                                               (cell-pos cell)
                                                               new-cells))
                                 (when (plusp chg)
                                   (incf changes chg)
                                   (collect cell)))))
                 (when (plusp changes)
                   (flet
                       ((cell-class-fn (cell-num)
                          (format nil "~:[~;cause~] ~:[~;effect~] ~:[~;expl~]"
                                  (member cell-num set
                                          :key #'cell-pos)
                                  (member cell-num changed)
                                  (member cell-num (area-cells area))))
                        (title ()
                          (cl-who:with-html-output-to-string (*standard-output*)
                            (:span 
                             (:b 
                              "The numbers "
                              (cl-who:fmt "~{~a, ~^~}"
                                          (allowed-numbers superset)))
                             " must complete the set of cells "
                             (:span :class "cause"
                              (cl-who:fmt "~{~d~^, ~}" 
                                          (sort (mapcar #'cell-pos set)
                                                #'<)))
                             ", so these cannot be in other parts of "
                             (:span :class "expl"
                              (cl-who:str (area-name area))
                              (cl-who:fmt " (~{~d~^, ~})" (sort (area-cells area) #'<)))
                             ))))
                     (signal 'return-a-step 
                             :kind       'set-of-cells-complete
                             :changes    changes
                             :callback   #'cell-class-fn
                             :title      #'title
                             :new-state  new-cells)))))))
         (job (set1 set2)
           (check-complete set1 set2)
           (check-complete set2 set1)))
        (let* ((cells (mapcar (lambda (pos)
                                (board-cell board pos))
                              (area-cells area)))
               (filtered (remove 1 cells
                                 :key #'how-many-possibilities-left)))
          ;; Now try to find small sets of cells
          ;; where the number of bits is equal to the number of cells
          (map-permutations* #'job filtered 
                                          :sets 2
                                          :min-length 2)
          ))))


(defsolver numbers-fill-set (board)
  "Inverse to SET-OF-CELLS-COMPLETE:
  if the numbers (1 2 3) can only be in 3 cells in an area,
  NO OTHER numbers can be in these cells"
  (let ((new-cells (copy-board-cells board)))
    (labels 
        ((check-filled-for-nums (bitmask set-bits)
           (dolist (area (areas-for-board board))
             (let* ((cells (remove-if-not (lambda (c)
                                            (any-of-these-possible? bitmask c nil))
                                          (area-cells area))))
               (when (and (= set-bits (length cells))
                          (format *trace-output* "~&area ~s,~
                                  cells ~s,~
                                  bits ~d, mask ~x~%"
                                  (area-cells area)
                                  cells
                                  set-bits
                                  bitmask)
                          (let* ((changes 0)
                                 (changed (iter (for cell in cells)
                                            (for chg = (only-keep-mask bitmask 
                                                                       cell
                                                                       new-cells))
                                            (when (plusp chg)
                                              (incf changes chg)
                                              (collect cell)))))
                            (when changed
                              (flet
                                  ((cell-class-fn (cell-num)
                                     (format nil "~:[~;cause~] ~:[~;effect~]"
                                             (member cell-num cells)
                                             (member cell-num changed)))
                                   (title ()
                                     (cl-who:with-html-output-to-string (*standard-output*)
                                       (:span 
                                        (:b 
                                         "The numbers "
                                         (cl-who:fmt "~{~a, ~^~}"
                                                     (allowed-numbers bitmask)))
                                        " fill the cells "
                                        (:span :class "cause"
                                         (cl-who:fmt "~{~d~^, ~}" 
                                                     (sort cells #'<)))
                                        ", so there cannot be anything else in "
                                        (:span :class "expl"
                                         (cl-who:fmt " (~{~d~^, ~})" 
                                                     (sort changed #'<)))
                                        ))))
                                (signal 'return-a-step 
                                        :kind       'numbers-fill-set
                                        :changes    changes
                                        :callback   #'cell-class-fn
                                        :title      #'title
                                        :new-state  new-cells))))))))))
      (dotimes (i size)
        ;; Zero mask, or single number, don't make sense
        (let ((set-bits (logcount i)))
          (when (>= set-bits 2)
            (check-filled-for-nums i set-bits)))))))


(defsolver restrict-by-neighbours (board)
  "If within an area some number is limited to an overlapping area,
  it must not be in other parts of the second area.
  Eg. if a digit 3 must be in the first row of a quadrant,
  it must not be anywhere else in this row."
  ;; This must happen with a row or column to a quad- or irregular area -
  ;; a row and a column intersect in only one place.
  ;; For a single place this degenerates to ONLY-ONE-PLACE-LEFT-IN-AREA. 
  (flet
    ((check (area-1 area-2)
       ;; Get overlapping region
       (let* ((common-cells (intersection (area-cells area-1)
                                          (area-cells area-2))))
         (when common-cells
           ;; Get non-overlapping parts
           (let* ((cells-1 (set-difference (area-cells area-1) common-cells))
                  (cells-2 (set-difference (area-cells area-2) common-cells))
                  ;; Contents
                  (nums-1 (cell-union cells-1 board))
                  (nums-2 (cell-union cells-2 board))
                  ;; Numbers to ignore
                  (to-ignore (cell-union 
                               (mapcar (lambda (pos)
                                         (let ((cell (board-cell board pos)))
                                           (if (cell-done? cell)
                                               (cell-possible cell)
                                               0)))
                                       (append (area-cells area-1)
                                               (area-cells area-2))))))
             (flet 
                 ((left-over (nums area nums-other a-other)
                    (let* ((only-in-sub (numbers-not-in-second nums nums-other))
                           (interesting (numbers-not-in-second only-in-sub to-ignore)))
                      (when (plusp interesting)
                        (let* ((new-state (copy-board-cells board))
                               (changes 0)
                               (changed (iter (for pos in area)
                                          (for chg = (disallow-multiple interesting pos new-state))
                                          (when (plusp chg)
                                            (incf changes chg)
                                            (collect pos))))
                               (numbers (allowed-numbers interesting)))
                          (flet
                              ((cell-class-fn (cell-num)
                                 (format nil "~:[~;cause~] ~:[~;effect~] ~:[~;expl~]"
                                         (member cell-num a-other)
                                         (member cell-num common-cells)
                                         (member cell-num changed)))
                               (title ()
                                 (cl-who:with-html-output-to-string (*standard-output*)
                                   (:span 
                                    (:b 
                                     (cl-who:fmt "The number~p ~{~a~^, ~}" (length numbers) numbers))
                                    " cannot be in "
                                    (:span :class "cause"
                                     (cl-who:fmt "the cell~p ~{~d~^, ~}" (length a-other) (sort a-other #'<)))
                                    (cl-who:fmt ", so ~:[it~;they~] must be contained in "
                                                (/= 1 (length numbers)))
                                    (:span :class "effect"
                                     "the cells "
                                     (cl-who:fmt "~{~d~^, ~}" common-cells))
                                    " and can be removed from "
                                    (:span :class "expl"
                                     (cl-who:fmt "~{~d~^, ~}" (sort changed #'<)))))))
                            #+(or)
                            (format t "0~d 0~o 0~o, in ~s ~s ~s~%"
                                    only-in-sub interesting to-ignore area common-cells a-other)
                            (signal 'return-a-step
                                    :kind 'restrict-by-neighbours
                                    :changes changes
                                    :callback  #'cell-class-fn
                                    :title     #'title
                                    :new-state new-state)))))))
               (left-over nums-1 cells-1 nums-2 cells-2)
               (left-over nums-2 cells-2 nums-1 cells-1)))))))
       (iter (for area in-vector (board-areas board))
         (dolist (r *row-areas*)
           (check area r))
         (dolist (c *col-areas*)
           (check area c)))))


(defun run-solvers (board)
  (iter (for algo in (reverse *solver-algos*))
    (handler-case
        (funcall algo board)
      (return-a-step (c)
        (collect c)))))

(defun run-to-completion (board)
  ;; simple first
  (let ((algos (reverse *solver-algos*)))
  (labels 
      ((next (b steps)
;         (format t "~%~d steps;~% ~s~%" (length steps) (board-cell b 29))
;         (serialize-board b *standard-output*)
;         (terpri)
;         (print-board-ascii b)
         (cond
           ((board-done? b)
            (return-from run-to-completion 
              (values (with-output-to-string (s)
                        (print-board-ascii b s))
                      (with-output-to-string (s)
                        (serialize-board b s))
                      #+(or)
                      (nreverse steps))))
           (t
            (when (> (length steps) 100)
              (return-from run-to-completion nil))
            (dolist (algo algos)
              (handler-case
                  (funcall algo b)
                (return-a-step (s)
;                  (format t "~&got ~s~%" (funcall (solution-title s)))
;         (format t "now ~s~%"  (aref (solution-new-state s) 29))
                  (next (make-board :cells (solution-new-state s)
                                    :areas (board-areas b))
                        (cons s steps)))))))))
    (next (copy-board board)
          ()))))
