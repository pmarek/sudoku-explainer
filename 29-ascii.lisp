(in-package :sudoku)

(defun print-board-ascii (board &optional (stream *standard-output*))
  (dotimes (row size)
    (dotimes (col size)
      (let* ((p (xy-to-linear col row))
             (cell (board-cell board p))
             (done? (cell-done? cell)))
        (format stream "~a"
                (if done?
                    (bit-to-name done?)
                    "."))))
    (format stream "~%")))


(alexandria:define-constant +areas-asciis+ "AbCeFgHiK"
  :test #'string=)

(defun print-board-areas-ascii (board &optional (stream *standard-output*))
  (dotimes (row size)
    (dotimes (col size)
      (let* ((p (xy-to-linear col row))
             (cell (board-cell board p)))
        (princ (aref +areas-asciis+ (cell-area-idx cell)))))
    (format stream "~%")))

(alexandria:define-constant +pad-chars+ ".-_ #"
  :test #'string=)
(alexandria:define-constant +eol-chars+ (list #\Newline #\Return)
  :test #'equal)


(defun load-areas-from-ascii (areas-string)
  (let ((areas-ht (make-hash-table :test #'eql))
        (areas-ordered ()))
    (with-input-from-string (stream areas-string)
      (iter
        (for row below size)
        (iter 
          (for col below size)
          (for ch = (read-char stream))
          ;; If at the start of a line, just eat it.
          (when (find ch +eol-chars+)
            (let ((next (peek-char nil stream nil nil)))
              ;; The "other" CRLF
              (when (and (member next +eol-chars+)
                         (char/= next ch))
                (read-char stream)))
            (when (/= 0 col)
              (leave))
            (setf ch (read-char stream)))
          (cond
            ((or (char<= #\a ch #\z)
                 (char<= #\A ch #\Z))
             (pushnew ch areas-ordered 
                      :test #'char=)
             (push (xy-to-linear col row)
                   (gethash ch areas-ht)))
            (t
             (error "invalid char ~s" ch))))))
    ;;
    #+(or)
    (when (/= size (hash-table-count areas-ht))
      (error "wrong number of area designators: ~s" areas-ordered))
    (map 'vector (lambda (ch i)
                   (let* ((cells (gethash ch areas-ht))
                          (have (length cells)))
                     (unless (= size have)
                       (error "wrong number of cells in area ~s: ~s" ch cells))
                     (make-area :name (format nil "area-~d" i)
                                :cells cells)))
                      (reverse areas-ordered)
                      (alexandria:iota size))))

(defun load-board-from-ascii (board-string areas)
  (let ((board (make-full-board areas)))
    (with-input-from-string (stream board-string)
      (iter 
        (for row below size)
        (iter 
          (for col below size)
          (for pos = (xy-to-linear col row))
          (for ch = (read-char stream nil #\.))
          (when (find ch +eol-chars+)
            (let ((next (peek-char nil stream nil nil)))
              ;; The "other" CRLF
              (when (and (member next +eol-chars+)
                         (char/= next ch))
                (read-char stream nil nil)))
            (when (/= 0 col)
              (leave))
            (setf ch (read-char stream)))
          (cond
            ((char<= #\1 ch #\9)
             (set-number (- (char-code ch)
                            (char-code #\1))
                         pos
                         board))
            ((find ch +pad-chars+)
             t)
            (t
             (error "invalid char ~s" ch))))))
    board))

(defun load-from-ascii (board-string areas-string)
  (load-board-from-ascii 
    board-string
    (if (and areas-string
             (> (length areas-string) +total-cells+))
        (load-areas-from-ascii areas-string)
        (coerce *quad-areas* 'vector))))

