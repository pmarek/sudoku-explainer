(in-package :sudoku)


(defstruct area
  (name  nil :type string)
  ; list of (integer 0 #. size)
  (cells nil :type list))

(defvar *dummy-area* (make-area :name ""))

(defstruct cell
  (possible   0 :type fixnum)
  (done?    nil :type (or null (integer 0 #. size)))
  (area-idx   0 :type (integer  0 #. size))
  (pos       -1 :type (integer -1 #. +total-cells+))
  (area      *dummy-area* :type area))

(defvar *dummy-cell* (make-cell :possible 0
                                :done? nil 
                                :area-idx 0 
                                :area *dummy-area*))


(defmethod print-object ((cell cell) stream)
  (print-unreadable-object (cell stream :type t :identity nil)
    (format stream "~d~:[~;, done~]: p 0~3,'0o, area #~d ~s"
            (cell-pos cell)
            (cell-done? cell)
            (cell-possible cell)
            (cell-area-idx cell)
            (area-cells (cell-area cell)))))


(defmethod how-many-possibilities-left ((num fixnum))
  (values (logcount num)
          num))

(defmethod how-many-possibilities-left ((cell cell))
  (how-many-possibilities-left (cell-possible cell)))
                                 

(defmethod only-one-possibility-left (cell)
  (multiple-value-bind (count num) (how-many-possibilities-left cell)
    (when (= 1 count)
      ;; A FFS (find first set) would be nice
      (round (log num 2)))))

(defmethod allowed-numbers (num)
  "Returns a list of user-readable data, and a second value of low-level data."
  (iter (for i below size)
    (when (is-allowed? i num nil)
      (collect (bit-to-name i) into user)
      (collect (ash 1 i) into low-level))
    (finally
      (return (values user low-level)))))
