(in-package :sudoku)


;; TODO: use inline-generics
(defmethod is-allowed? (number (pos fixnum) (board (eql nil)))
  (logbitp number pos))

(defmethod is-allowed? (number (cell cell) (board (eql nil)))
  (is-allowed? number
              (cell-possible cell)
              nil))

(defmethod is-allowed? (number pos (board board))
  (is-allowed? number
              (board-cell board pos)
              nil))

(defmethod is-allowed? (number pos (board array))
  (is-allowed? number
              (aref board pos)
              nil))

(defmethod any-of-these-possible? (numbers (possible fixnum) (board (eql nil)))
  (plusp (logand numbers possible)))

(defmethod any-of-these-possible? (numbers (cell cell) (board (eql nil)))
  (any-of-these-possible? numbers (cell-possible cell) board))

(defun numbers-not-in-second (sum subtrahend)
  "Returns a set of numbers in SUM that is not in SUBTRAHEND"
  (logandc2 sum subtrahend))


(defun disallow (number pos board)
  (let ((cell (board-cell board pos)))
    (setf (ldb (byte 1 number)
               (cell-possible cell))
          0)
    (assert (plusp (cell-possible cell)))))

(defun disallow-multiple (numbers pos board)
  "Returns number of changes (0 for none)"
  (let* ((cell (board-cell board pos))
         (old (cell-possible cell)))
    (setf (cell-possible cell)
          (numbers-not-in-second (cell-possible cell)
                                 numbers))
    (assert (plusp (cell-possible cell)))
    (logcount (logxor old
                      (cell-possible cell)))))


(defun only-keep-mask (mask pos board)
  (let* ((cell (board-cell board pos))
         (old (cell-possible cell))
         (new (logand mask
                      (cell-possible cell))))
    (cond
      ((/= new old)
       (setf (cell-possible cell) new)
       (logcount (logxor new old)))
      (t 0))))

(defun set-number (num pos board)
  "Returns a list of positions that have been modified
  and the number of changes"
  (assert (is-allowed? num pos board))
  (let ((changed (list pos))
        (changes 0)
        (cell (board-cell board pos)))
    (setf (cell-done? cell)    num
          (cell-possible cell) (ash 1 num))
    (dolist (area (areas-for-cell board pos))
      (dolist (c-pos (area-cells area))
        (unless (= c-pos pos)
          (when (is-allowed? num c-pos board)
            (push c-pos changed)
            (disallow num c-pos board)))))
    (values (nreverse changed)
            changes)))

#|

(defun list-of-integer (x)
  (integerp (first (the list x))))
(defun list-of-cell-p (x)
  (cell-p (first (the list x))))

(defmethod cell-union ((cells (satisfies list-of-integer)) (board (eql nil)))
  (apply #'logior 0 cells))

(defmethod cell-union ((cells (satisfies list-of-cell-p)) (board (eql nil)))
  (cell-union (mapcar #'cell-possible cells) nil))

(defmethod cell-union ((cells (satisfies list-of-integer)) (board board))
  (cell-union (mapcar (lambda (pos)
                        (board-cell board pos))
                      cells)
              nil))


|#

(defun cell-union (cells &optional board)
  (labels
      ((integer-list (x)
         (apply #'logior 0 x)))
    (cond
      ((and (null board)
            (every #'integerp cells))
       (integer-list cells))
      ((and (null board)
            (every #'cell-p cells))
       (integer-list 
         (mapcar #'cell-possible 
                 cells)))
      ((and (board-p board)
            (every #'integerp cells))
       (integer-list 
         (mapcar (lambda (pos)
                   (cell-possible
                     (board-cell board pos)))
                 cells)))
      (t
       (error "bad input")))))
