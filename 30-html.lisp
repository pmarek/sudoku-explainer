(in-package :sudoku)

(defvar *last* nil)

(defparameter +style+ "

     /* R4 pastel-1 */
     .area-0 { background-color: #FBB4AE; }
     .area-1 { background-color: #B3CDE3; }
     .area-2 { background-color: #CCEBC5; }
     .area-3 { background-color: #DECBE4; }
     .area-4 { background-color: #FED9A6; }
     .area-5 { background-color: #FFFFCC; }
     .area-6 { background-color: #E5D8BD; }
     .area-7 { background-color: #FDDAEC; }
     .area-8 { background-color: #F2F2F2; }

     table {
         border-collapse: collapse;
     }
     td {
         vertical-align: middle;
         text-align:center;
     }
     .name {
         color: #820;
         text-align: right; 
         padding-right: 3px;
     }
     .inner {
         border: 1px solid #777;
        min-width: 1em;
     }
     tr.outer {
        min-height: 1em;
     }
     .multiple {
         font-size: xx-small;
     }
     table.multiple td {
        // display: none;
     }
     table.board .title {
         width: min-content;
         height: 9em;
     }
     .dirty {
         background: yellow;
     }
     .removed {
         text-decoration: line-through;
         color:grey;
         visibility: hidden; 
     }
     .forbid {
         text-decoration: line-through;
     }
     .onlyone {
         font-size: xx-large;
     }
     /*
     td.inner > span, td.inner > span > span {
        width: 100%;
        height: 100%;
        display: block;
     } */
     input[name=n1]:checked ~ table .n1,   input[name=n1]:hover ~ table .n1,
     input[name=n2]:checked ~ table .n2,   input[name=n2]:hover ~ table .n2,
     input[name=n3]:checked ~ table .n3,   input[name=n3]:hover ~ table .n3,
     input[name=n4]:checked ~ table .n4,   input[name=n4]:hover ~ table .n4,
     input[name=n5]:checked ~ table .n5,   input[name=n5]:hover ~ table .n5,
     input[name=n6]:checked ~ table .n6,   input[name=n6]:hover ~ table .n6,
     input[name=n7]:checked ~ table .n7,   input[name=n7]:hover ~ table .n7,
     input[name=n8]:checked ~ table .n8,   input[name=n8]:hover ~ table .n8,
     input[name=n9]:checked ~ table .n9,   input[name=n9]:hover ~ table .n9
     {
         background: #ddd;
         color: #333;
         outline: 2px solid black;
     }
     .expl {
         background: repeating-linear-gradient(
                            135deg,
                            #E5D8BD80,
                            #E5D8BD80 1em,
                            #CCEBC500 1em,
                            #CCEBC500 2em
                            );
         /* There's no way(?) to remove that between td.expl, sadly
          * (to get a border around the area) --
          *    td.expl + td.expl { border-left: none; }
          * but not for the first one or TR above
         border: 2px solid darkgreen;  */
     }
     .effect {
/*         border: 2px solid orange;
         content: \"\";
         display: block;
         position: absolute;
         width:32px;
         height:32px; */
         background: repeating-linear-gradient(
                        60deg,
                        #DECBE4,
                        #DECBE4 1em,
                        #F2F2F2 1em,
                        #F2F2F2 2em
                        );
    }
    .cause {
        border: 2px solid red;
        /*
        background: repeating-linear-gradient(
                        135deg,
                        #FDDAEC00,
                        #E5D8BD00 1em,
                        #B3CDE380 1em,
                        #B3CDE380 2em
                        ),
repeating-linear-gradient(
                        45deg,
                        #FDDAEC80,
                        #E5D8BD80 1em,
                        #B3CDE300 1em,
                        #B3CDE300 2em
                        );
                        */
    }
    @media print {
        input, .removed, textarea {
            visibility: hidden;
        }
    }
    ")


(defun href-for-board (board)
  (format nil "?board=~a" 
          (with-output-to-string (s)
            (serialize-board board s))))

(defun bit-to-name (n)
  (1+ n))


(defun %print-cell (board pos)
  (with-html-output (*standard-output*)
    (let* ((cell (board-cell board pos))
           (poss (cell-possible cell))
           (done? (cell-done? cell)))
      (if done?
          (let ((n (bit-to-name done?)))
            (htm
              (:span
               :class (format nil "onlyone n~d" n)
               (str n))))
          (htm
            (:table
             :class "multiple"
             (dotimes (col sqrt-size)
               (htm
                 (:tr
                  (dotimes (row sqrt-size)
                    (htm
                      (:td
                       (str
                         (let* ((i (xy-to-linear row col sqrt-size))
                                (n-b (copy-board-cells board))
                                (allow (logbitp i poss)))
                           (ignore-errors
                             (set-number i pos n-b))
                           (htm
                             (:a
                              :class (format nil "~a n~d ~a" 
                                             (if allow "" "forbid")
                                             (1+ i)
                                             (if allow "" "removed"))
                              :href (href-for-board n-b)
                              (str (1+ i))))))))))))))))))

(defun print-board-html (board title cell-class-fn)
  (cl-who:with-html-output (*standard-output*)
    (:table
     :class "board"
     (:tr
      (:th
       :colspan size
       :class "title"
       (cl-who:str title)))
     (dotimes (row size)
       (cl-who:htm
         (:tr
          :class "outer"
          (:td
           :class "name"
           (str (xy-to-linear 0 row)))
          (dotimes (col size)
            (let ((p (xy-to-linear col row)))
              (htm
                (:td
                 :class (format nil "~a inner area-~d col-~d row-~d"
                                (if cell-class-fn
                                    (funcall cell-class-fn p)
                                    "")
                                (cell-area-idx (board-cell board p))
                                col
                                row)
                 (%print-cell board p))))))))))
    board)

(hunchentoot:define-easy-handler (solver :uri "/sudoku")
    ((board)
     (ascii) (areas))
  (let* ((board (or (and board (deserialize-board board))
                    (and ascii areas (load-from-ascii ascii areas)))))
    (cl-who:with-html-output-to-string (*standard-output*)
      (:head
       (:title "Sudoku-Explainer")
       (:style
        (cl-who:str +style+)))
      (cond
        (board
         (setf *last* board)
         (cl-who:htm
           (:div
            :class "highlight")
           (dotimes (i size)
             (let ((n (1+ i)))
               (cl-who:htm
                 (:input
                  :type "checkbox"
                  :name (format nil "n~d" n)
                  (cl-who:str n)))))
           (:table
            (:tr
             (:td
              (print-board-html board "Baseline" nil)
              (:div
               "Status quo"))
             (dolist (sol (run-solvers board))
               (cl-who:htm
                 (:td
                  (print-board-html (solution-new-state sol)
                                    (funcall (solution-title sol))
                                    (solution-cell-class sol))
                  (:div
                   (:a :href (href-for-board (solution-new-state sol))
                    "Accept")))))))
           (:hr)))
        (t
         (cl-who:htm
           (:ul
         (do-symbols (sym :sudoku)
           (let ((name (symbol-name sym)))
           (when (alexandria:starts-with-subseq "*EXAMPLE" name)
         (cl-who:htm
           (:li
            (:a :href (href-for-board (symbol-value sym))
            (cl-who:fmt "~:@(~a~)"
                        (subseq name 1 (- (length name) 1)))))))))))))
      (let ((b (or board (make-full-board (coerce *quad-areas* 'vector)))))
        (cl-who:htm
          (:form :action "?"
           (:textarea 
            :name "ascii" 
            :cols 13
            :rows 10
            (print-board-ascii b))
           (:textarea 
            :name "areas" 
            :cols 13
            :rows 10
            (print-board-areas-ascii b))
           (:input :type :submit :name "Load" :value "Load")))))))
