(in-package :sudoku)


(defvar *example-1*
  (load-from-ascii
    ".......8.
6....9...
.........
.7.......
.21.6..7.
..4.8....
........2
........6
.93...14."
"aabbccccc
aaabbbbcc
daaabeecc
dddabeeff
dhddbeeff
dhhheeeff
dhhmmmmmf
hhhpppmmf
ppppppmmf"))

(with-output-to-string (s)
  (serialize-board *example-1* s))


(defvar *example-hard* (load-from-ascii
"8__...___
__36..___
_7_.9.2__
.5.__7...
..._457..
...1__.3.
__1..._68
__85.._1_
_9_...4__" nil))


(with-output-to-string (s)
  (serialize-board *example-hard* s))


(defvar *example-2* (load-from-ascii
"___.247__
_1_..._5_
__8...__3
..25__..7
.4._3_.8.
6..__91..
7__...9__
_3_..._6_
__581.___" nil))




(defvar *example-3* (load-from-ascii
".........
.1.4.3.5.
....3....
..6...3..
52.319.84
2.......1
.91.7.83.
....4....
....2...."

"abbbbcccc
aaaabcddc
eabbbbdfc
eaggdddfc
eaagggdfc
eeeegddff
ehehgggif
hhhhhhiif
hiiiiiiff"))

(defvar *example-4* (load-from-ascii
"....6....
..7....1.
....3....
.8......4
...2.....
.....9...
9.5....7.
.6...715.
3..5....8"

"aabbbbccc
abbbdcccc
aabbddecc
affdddeee
aafddggee
alffdggee
lllfggmem
lllfgmmmm
llffggmmm"))

(defparameter *example-5* 
  (let* ((b (make-full-board (coerce *quad-areas* 'vector)))
        (area-cells (list 0 27 54)))
    (loop for i from 3 to 9
      do (dolist (c area-cells)
        (disallow i c b)))
    b))



(setf *last* *example-1*)

;; http://localhost:8124/sudoku?board=0A40G0MY204W63560T8g8tJ!GlKQGmJ.0W2J33Aj8tB9AiFCGDSI2J6q6|8tVaZFJOJ.SINDSc617yUfYKf8h6SGicMJSG7zU6Xtc2h5QKjKiin!VbVSZ7f8f8SGlApnxDuytXxDuSb9pMklpH.byl.bwqt*bd.S_5xT.yyc.yp-q4dD
