(in-package :sudoku)

(defconstant sqrt-size 3)

(defconstant size (* sqrt-size   sqrt-size))
(defconstant +total-cells+ (* size size))


(defconstant +cell-all-possible+ (1- (ash 1 size)))

;; 1 bit (LSB) is used as "finalize" information, one as "dirty".
(defconstant +finalized-mask+ (ash 1       size))
(defconstant +region-shift+           (+ 2 size))
(defconstant +bits-for-region+             size)
(defconstant +end-of-saved-bits+      (+ +region-shift+ +bits-for-region+))
(defconstant +dirty-mask+     (ash 1  +end-of-saved-bits+))
(defconstant +bits-per-cell+          (+ +end-of-saved-bits+ 1))
(defconstant cell-max-value    (1- (ash 1 +bits-per-cell+)))



