(cl:in-package #:asdf-user)

#+quicklisp
(ql:quickload '(:quux-hunchentoot 
                :cl-who
                :cl-interpol
                :iterate
                :hunchentoot
                :parse-number
                :alexandria))

(defsystem :sudoku
  :description "sudoku-solver"
  :license "LGPL-3"
  :author "Philipp Marek <philipp@marek.priv.at>"
  :depends-on (:quux-hunchentoot 
               :cl-who
               :cl-interpol
               :iterate
               :hunchentoot
               :parse-number
               :alexandria)
  :around-compile (lambda (thunk)
                    (cl-interpol:enable-interpol-syntax)
                    (funcall thunk))
  :serial t
  :in-order-to ((test-op (test-op :grants4companies/tests)))
  :components ((:file "00-package")
               (:file "01-consts")
               (:file "10-base69")
               (:file "20-cell")
               (:file "22-areas")
               (:file "23-board")
               (:file "25-cell-ops")
               (:file "29-ascii")
               (:file "30-html")
               (:file "50-solvers")
               (:file "80-examples")
               (:file "99-server")
               ))

